#!/bin/bash

# remove broken gnome-shell-extension-desktop-icons package
sudo apt remove gnome-shell-extension-desktop-icons

# install nemo
sudo apt install nemo

# setup autostart for nemo-desktop
path="~/.config/autostart/nemo-autostart-with-gnome.desktop"

sudo sh -c "echo '[Desktop Entry]'" >> $path
sudo sh -c "echo 'Type=Application'" >> $path
sudo sh -c "echo 'Name=Nemo'" >> $path
sudo sh -c "echo 'Comment=Start Nemo desktop at log in'" >> $path
sudo sh -c "echo 'Exec=nemo-desktop'" >> $path
sudo sh -c "echo 'OnlyShowIn=GNOME;'" >> $path
sudo sh -c "echo 'AutostartCondition=GSettings org.nemo.desktop show-desktop-icons'" >> $path
sudo sh -c "echo 'X-GNOME-AutoRestart=true'" >> $path
sudo sh -c "echo 'NoDisplay=true'" >> $path

# set to false so desktop icons can be moved
gsettings set org.nemo.desktop use-desktop-grid false

# reboot to apply changes
sudo reboot
