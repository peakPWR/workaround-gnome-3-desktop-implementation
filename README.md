# Workaround GNOME 3 Desktop implementation

This is a shell script implementation of the workaround shown in https://gitlab.gnome.org/GNOME/nautilus/-/issues/158#alternative-solution for the broken Nautilus desktop feature in the current Ubuntu LTS Version (20.04).
It removes the gnome-shell-extension-desktop-icons and uses nemo-desktop for the desktop icons.

This is for everyone that is looking for a quick fix for issues like:
- not beeing able to copy files on the desktop via CTRL+C
- drag and drop not working on desktop
- ...
